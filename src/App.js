import React, { Component } from 'react';
import './App.css';
import Toggler from './comp/Toggler'
import TogglerItem from './comp/TogglerItem'
import MyInput from './comp/MyInput'

class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
          data: {
              userName: '',
              pass: '',
              age: '',
              faLang: '',
              tog: 'left',
              gen: 'male'
          }
      }
  }

  handlerChange = (event) => {
      const target = event.target;
      const name = target.name;
      const val = target.value;
      this.setState({
          data: {
              ...this.state.data,
              [name]: val
          }
      });
  };
  outputHandler = () => {
      const { userName, pass, age, faLang, tog, gen } = this.state.data;
      let userData = `
        %c User data: %c 
        Name: ${userName}\n
        Password: ${pass}\n
        Gender: ${gen}\n
        Age: ${age}\n
        Favorite language: ${faLang}\n
        Layout: ${tog}\n
      `;
      const style = 'background: #f00; color: #fff';
      const style1 = 'background: #b1b1b1; color: #000';
      console.log(userData, style, style1);
  };

  render() {
    const { userName, pass, age, faLang, tog, gen } = this.state.data;
    return (
      <form className="App">
          <MyInput
            type="text"
            name="userName"
            placeholder="Name"
            value={userName}
            handler={this.handlerChange}
          />
          <MyInput
            type="password"
            name="pass"
            placeholder="Password"
            value={pass}
            handler={this.handlerChange}
          />

          <Toggler hendler={this.handlerChange} curActive={gen}>
              <TogglerItem name="gen" val="male"/>
              <TogglerItem name="gen" val="female"/>
          </Toggler>

          <MyInput
            type="number"
            name="age"
            placeholder="Your age"
            value={age}
            handler={this.handlerChange}
          />

          <Toggler hendler={this.handlerChange} curActive={tog}>
              <TogglerItem name="tog" val="left"/>
              <TogglerItem name="tog" val="centre"/>
              <TogglerItem name="tog" val="right"/>
          </Toggler>

          <MyInput
            type="text"
            name="faLang"
            placeholder="Favorite language"
            value={faLang}
            handler={this.handlerChange}
          />
          <button type="button" onClick={this.outputHandler}>Output data</button>
      </form>
    );
  }
}

export default App;
