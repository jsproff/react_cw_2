import React from 'react'

const Toggler = ({ children, hendler, curActive }) => {
    return (
        <div className="wrapper">
            {
                React.Children.map(
                    children,
                    (ChildrenItem) => {
                        if (ChildrenItem.props.val === curActive) {
                            return React.cloneElement(ChildrenItem, {
                                name: ChildrenItem.props.name,
                                val: ChildrenItem.props.val,
                                isActive: true,
                                toggle: hendler
                            })
                        } else {
                            return React.cloneElement(ChildrenItem, {
                                name: ChildrenItem.props.name,
                                val: ChildrenItem.props.val,
                                toggle: hendler
                            })
                        }
                    }
                )
            }
        </div>
    )

};

export default Toggler;