import React from 'react'

const TogglerItem = (props) => {
    return(
        <button type="button" className={props.isActive ? 'btn active' : 'btn'} onClick={props.toggle} name={props.name} value={props.val}>
            {props.val}
        </button>
    )
};
export default TogglerItem;