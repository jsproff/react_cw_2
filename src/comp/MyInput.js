import React from 'react';

const MyInput = ({ type, name, value, placeholder, handler }) => {
    return(
        <label>
            {name}
            <input
                name={name}
                type={type}
                placeholder={placeholder}
                onChange={handler}
                value={value}
            />
        </label>
    )
};

export default MyInput;